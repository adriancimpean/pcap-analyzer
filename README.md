--USAGE:

pickle / analyze / plot

Pickle - read pcap file and filters and stores interesting packets

Analyze - read interesting packets from pickle file and print packet information

Plot - displays a plot with ack number and TCP window size

--RUN SCRIPT

Pickle: 
python3 main.py pickle --pcap file_name.pcap --out pickle_file_name.dat --client client_ip:port --server server_ip:port 

Analyze:
python3 main.py analyze --input pickle_file_name.dat --client client_ip:port --server server_ip:port

Plot:
python3 main.py plot --input pickle_file_name.dat --client client_ip:port --server server_ip:port

--DEPENDENCIES
argparse
scapy
pickle
pandas
matplotlib